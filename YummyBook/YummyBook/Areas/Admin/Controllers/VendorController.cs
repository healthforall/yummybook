﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YummyBook.Areas.Admin.Controllers
{
    public class VendorController : Controller
    {
        // GET: Admin/Vendor
        public ActionResult FoodVendors()
        {
            return View();
        }
        public ActionResult CreateVendor()
        {
            return View();
        }
    }
}