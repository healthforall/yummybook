﻿using System.Web.Mvc;

namespace YummyBook.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
               "Admin_Login",
               "Admin/Login",
               new { controller = "Session", action = "Login", id = UrlParameter.Optional }
            );
            context.MapRoute(
               "Admin_OrderConfirmation",
               "Admin/OrderConfirmation",
               new { controller = "Order", action = "OrderConfirmation", id = UrlParameter.Optional }
            );
            context.MapRoute(
               "Admin_Menu",
               "Admin/Menu",
               new { controller = "Menu", action = "Menu", id = UrlParameter.Optional }
            );
            context.MapRoute(
               "Admin_CreateVendor",
               "Admin/CreateVendor",
               new { controller = "Vendor", action = "CreateVendor", id = UrlParameter.Optional }
            );
            context.MapRoute(
               "Admin_FoodVendor",
               "Admin/FoodVendors",
               new { controller = "Vendor", action = "FoodVendors", id = UrlParameter.Optional }
            );
            context.MapRoute(
               "Admin_User",
               "Admin/User",
               new { controller = "User", action = "User", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Admin_Home",
                "Admin/Home",
                new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}