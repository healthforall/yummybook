﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YummyBook.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class YummyBookEntities : DbContext
    {
        public YummyBookEntities()
            : base("name=YummyBookEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Food> Foods { get; set; }
        public virtual DbSet<FoodMapping> FoodMappings { get; set; }
        public virtual DbSet<FoodVendor> FoodVendors { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderMapping> OrderMappings { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}
